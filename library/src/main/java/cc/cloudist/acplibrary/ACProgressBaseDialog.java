package cc.cloudist.acplibrary;


import ohos.agp.utils.Point;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

public abstract class ACProgressBaseDialog extends CommonDialog  {

    public ACProgressBaseDialog(Context context) {
        super(context);
    }
    protected int getMinimumSideOfScreen(Context context) {

        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point size = new Point();
        display.get().getSize(size);
        return  Math.min(size.getPointXToInt(), size.getPointYToInt());
    }

}
