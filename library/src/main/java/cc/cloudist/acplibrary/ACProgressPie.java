package cc.cloudist.acplibrary;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.TimerTask;

import cc.cloudist.acplibrary.views.PieView;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class ACProgressPie extends ACProgressBaseDialog {

    private final Builder mBuilder;
    private PieView mPieView;
    private Timer mTimer;
    private int mSpinCount = 0;
    private boolean CanceledOnTouchOutside;

    public boolean isCanceledOnTouchOutside() {
        return CanceledOnTouchOutside;
    }

    public void setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
        CanceledOnTouchOutside = canceledOnTouchOutside;
    }

    private ACProgressPie(Builder builder) {
        super(builder.mContext);
        mBuilder = builder;
    }

    public void show(ACProgressPie acProgressPie) {
        if (mPieView == null) {
            int size = new BigDecimal(getMinimumSideOfScreen(mBuilder.mContext) ).multiply(new BigDecimal( mBuilder.mSizeRatio)).intValue();
            mPieView = new PieView(mBuilder.mContext, size, mBuilder.mBackgroundColor, mBuilder.mBackgroundAlpha, mBuilder.mBackgroundCornerRadius
                    , mBuilder.mRingBorderPadding
                    , mBuilder.mRingThickness, mBuilder.mRingColor, mBuilder.mRingAlpha
                    , mBuilder.mPieColor, mBuilder.mPieAlpha);
        }
        mTimer=new Timer();
        mPieView.setTouchEventListener((component, touchEvent) -> {
            if (CanceledOnTouchOutside) {
                acProgressPie.hide();
                mTimer.cancel();
            }
            return false;
        });
        super.setContentCustomComponent(mPieView);
        this.setTransparent(true);
        super.show();
        if (mBuilder.mUpdateType == ACProgressConstant.PIE_AUTO_UPDATE) {
            long delay = (long) (1000 / mBuilder.mSpeed);

            mTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    int result = mSpinCount % (mBuilder.mPieces + 1);
                    mPieView.updateAngle(360f / mBuilder.mPieces * result);
                    if (result == 0) {
                        mSpinCount = 1;
                    } else {
                        mSpinCount++;
                    }
                }
            }, delay, delay);
        }
    }

    public void setPiePercentage(float percentage) {
        if (mBuilder.mUpdateType == ACProgressConstant.PIE_MANUAL_UPDATE && mPieView != null) {
            mPieView.updateAngle(360 * percentage);
        }
    }

    public static class Builder {

        private Context mContext;

        private float mSizeRatio = 0.25f;

        private int mBackgroundColor = Color.BLACK.getValue();
        private float mBackgroundCornerRadius = 20f;
        private float mBackgroundAlpha = 0.3f;

        private int mRingColor = Color.WHITE.getValue();
        private float mRingAlpha = 0.9f;

        private float mRingBorderPadding = 0.2f;
        private int mRingThickness = 3;

        private int mPieColor = Color.WHITE.getValue();
        private float mPieAlpha = 0.5f;


        private float mSpeed = 6.67f;
        private int mPieces = 100;

        private int mUpdateType = ACProgressConstant.PIE_AUTO_UPDATE;

        public Builder(Context context) {
            mContext = context;
        }

        public Builder sizeRatio(float ratio) {
            mSizeRatio = ratio;
            return this;
        }

        public Builder bgColor(int color) {
            mBackgroundColor = color;
            return this;
        }

        public Builder bgAlpha(float alpha) {
            mBackgroundAlpha = alpha;
            return this;
        }

        public Builder bgCornerRadius(float cornerRadius) {
            mBackgroundCornerRadius = cornerRadius;
            return this;
        }

        public Builder ringColor(int color) {
            mRingColor = color;
            return this;
        }

        public Builder ringAlpha(float alpha) {
            mRingAlpha = alpha;
            return this;
        }

        public Builder ringBorderPadding(float padding) {
            mRingBorderPadding = padding;
            return this;
        }

        public Builder ringThickness(int thickness) {
            mRingThickness = thickness;
            return this;
        }

        public Builder pieColor(int color) {
            mPieColor = color;
            return this;
        }

        public Builder pieAlpha(float alpha) {
            mPieAlpha = alpha;
            return this;
        }


        public Builder speed(float speed) {
            mSpeed = speed;
            return this;
        }

        public Builder pieces(int pieces) {
            mPieces = pieces;
            return this;
        }

        public Builder updateType(int updateType) {
            mUpdateType = updateType;
            return this;
        }

        public ACProgressPie build() {
            return new ACProgressPie(this);
        }

    }
}
