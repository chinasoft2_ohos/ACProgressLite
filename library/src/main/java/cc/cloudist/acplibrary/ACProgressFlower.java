package cc.cloudist.acplibrary;

import cc.cloudist.acplibrary.views.FlowerView;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Timer;
import java.util.TimerTask;

public class ACProgressFlower extends ACProgressBaseDialog{

    private Builder mBuilder;
    private FlowerView mFlowerView;

    private int mSpinCount = 0;
    private Timer mTimer;
    private boolean CanceledOnTouchOutside;

    public boolean isCanceledOnTouchOutside() {
        return CanceledOnTouchOutside;
    }

    public void setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
        CanceledOnTouchOutside = canceledOnTouchOutside;
    }

    private ACProgressFlower(Builder builder) {
        super(builder.mContext);
        mBuilder = builder;

    }


    public void show(ACProgressFlower acProgressFlower) {
        if (mFlowerView == null) {
            int size = (int) (getMinimumSideOfScreen(mBuilder.mContext) * mBuilder.mSizeRatio);

            int windowWidth = (int) Utils.getScreenWidth(mBuilder.mContext);

            mFlowerView = new FlowerView(mBuilder.mContext, size, windowWidth, mBuilder.mBackgroundColor, mBuilder.mBackgroundAlpha, mBuilder.mBackgroundCornerRadius
                    , mBuilder.mPetalThickness, mBuilder.mPetalCount, mBuilder.mPetalAlpha, mBuilder.mBorderPadding, mBuilder.mCenterPadding
                    , mBuilder.mThemeColor, mBuilder.mFadeColor
                    , mBuilder.mText, mBuilder.mTextSize, mBuilder.mTextColor, mBuilder.mTextAlpha, mBuilder.mTextMarginTop);
        }
        setContentCustomComponent(mFlowerView);
        mFlowerView.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                if (CanceledOnTouchOutside) {
                    acProgressFlower.hide();
                    mTimer.cancel();
                }

                return false;
            }
        });
        this.setTransparent(true);
        long delay = (long) (1000 / mBuilder.mSpeed);
        mTimer = new Timer();
        super.show();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                int result = mSpinCount % mBuilder.mPetalCount;
                if (mBuilder.mDirection == ACProgressConstant.DIRECT_CLOCKWISE) {
                    mFlowerView.updateFocusIndex(result);
                } else {
                    mFlowerView.updateFocusIndex(mBuilder.mPetalCount - 1 - result);
                }
                if (result == 0) {
                    mSpinCount = 1;
                } else {
                    mSpinCount++;
                }
            }
        }, delay, delay);
    }

    public static class Builder {

        private Context mContext;
        private float mSizeRatio = 0.25f;
        private float mBorderPadding = 0.55f;
        private float mCenterPadding = 0.27f;

        private int mBackgroundColor = 0xFF000000;
        private int mThemeColor = 0xFF444444;
        private int mFadeColor = 0xFFFFFFFF;

        private int mPetalCount = 12;
        private int mPetalThickness = 9;
        private float mPetalAlpha = 0.5f;

        private float mBackgroundCornerRadius = 20f;
        private float mBackgroundAlpha = 0.5f;

        private int mDirection = ACProgressConstant.DIRECT_ANTI_CLOCKWISE;
        private float mSpeed = 9f;

        private String mText = null;
        private int mTextColor = 0xFFFFFFFF;
        private float mTextAlpha = 0.5f;
        private int mTextSize = 40;
        private int mTextMarginTop = 40;



        public float getSizeRatio() {
            return mSizeRatio;
        }

        public void setSizeRatio(float mSizeRatio) {
            this.mSizeRatio = mSizeRatio;
        }

        public float getBorderPadding() {
            return mBorderPadding;
        }

        public void setBorderPadding(float mBorderPadding) {
            this.mBorderPadding = mBorderPadding;
        }

        public float getCenterPadding() {
            return mCenterPadding;
        }

        public void setCenterPadding(float mCenterPadding) {
            this.mCenterPadding = mCenterPadding;
        }

        public int getBackgroundColor() {
            return mBackgroundColor;
        }

        public void setBackgroundColor(int mBackgroundColor) {
            this.mBackgroundColor = mBackgroundColor;
        }

        public int getThemeColor() {
            return mThemeColor;
        }

        public void setThemeColor(int mThemeColor) {
            this.mThemeColor = mThemeColor;
        }

        public int getFadeColor() {
            return mFadeColor;
        }

        public void setFadeColor(int mFadeColor) {
            this.mFadeColor = mFadeColor;
        }

        public int getPetalCount() {
            return mPetalCount;
        }

        public void setPetalCount(int mPetalCount) {
            this.mPetalCount = mPetalCount;
        }

        public int getPetalThickness() {
            return mPetalThickness;
        }

        public void setPetalThickness(int mPetalThickness) {
            this.mPetalThickness = mPetalThickness;
        }

        public float getPetalAlpha() {
            return mPetalAlpha;
        }

        public void setPetalAlpha(float mPetalAlpha) {
            this.mPetalAlpha = mPetalAlpha;
        }

        public float getBackgroundCornerRadius() {
            return mBackgroundCornerRadius;
        }

        public void setBackgroundCornerRadius(float mBackgroundCornerRadius) {
            this.mBackgroundCornerRadius = mBackgroundCornerRadius;
        }

        public float getBackgroundAlpha() {
            return mBackgroundAlpha;
        }

        public void setBackgroundAlpha(float mBackgroundAlpha) {
            this.mBackgroundAlpha = mBackgroundAlpha;
        }

        public int getDirection() {
            return mDirection;
        }

        public void setDirection(int mDirection) {
            this.mDirection = mDirection;
        }

        public float getSpeed() {
            return mSpeed;
        }

        public void setSpeed(float mSpeed) {
            this.mSpeed = mSpeed;
        }

        public String getText() {
            return mText;
        }

        public void setText(String mText) {
            this.mText = mText;
        }

        public int getTextColor() {
            return mTextColor;
        }

        public void setTextColor(int mTextColor) {
            this.mTextColor = mTextColor;
        }

        public float getTextAlpha() {
            return mTextAlpha;
        }

        public void setTextAlpha(float mTextAlpha) {
            this.mTextAlpha = mTextAlpha;
        }

        public int getTextSize() {
            return mTextSize;
        }

        public void setTextSize(int mTextSize) {
            this.mTextSize = mTextSize;
        }

        public int getTextMarginTop() {
            return mTextMarginTop;
        }

        public void setTextMarginTop(int mTextMarginTop) {
            this.mTextMarginTop = mTextMarginTop;
        }

        public Builder(Context context) {
            mContext = context;
        }


        public Builder sizeRatio(float ratio) {
            mSizeRatio = ratio;
            return this;
        }

        public Builder borderPadding(float padding) {
            mBorderPadding = padding;
            return this;
        }

        public Builder centerPadding(float padding) {
            mCenterPadding = padding;
            return this;
        }

        public Builder bgColor(int bgColor) {
            mBackgroundColor = bgColor;
            return this;
        }

        public Builder themeColor(int themeColor) {
            mThemeColor = themeColor;
            return this;
        }

        public Builder fadeColor(int fadeColor) {
            mFadeColor = fadeColor;
            return this;
        }

        public Builder petalCount(int petalCount) {
            mPetalCount = petalCount;
            return this;
        }

        public Builder petalThickness(int thickness) {
            mPetalThickness = thickness;
            return this;
        }

        public Builder petalAlpha(float alpha) {
            mPetalAlpha = alpha;
            return this;
        }

        public Builder bgCornerRadius(float cornerRadius) {
            mBackgroundCornerRadius = cornerRadius;
            return this;
        }

        public Builder bgAlpha(float alpha) {
            mBackgroundAlpha = alpha;
            return this;
        }

        public Builder direction(int direction) {
            mDirection = direction;
            return this;
        }

        public Builder speed(float speed) {
            mSpeed = speed;
            return this;
        }

        public Builder text(String text) {
            mText = text;
            return this;
        }

        public Builder textSize(int textSize) {
            mTextSize = textSize;
            return this;
        }

        public Builder textColor(int textColor) {
            mTextColor = textColor;
            return this;
        }

        public Builder textAlpha(float textAlpha) {
            mTextAlpha = textAlpha;
            return this;
        }

        public Builder textMarginTop(int textMarginTop) {
            mTextMarginTop = textMarginTop;
            return this;
        }


        public ACProgressFlower build() {
            return new ACProgressFlower(this);
        }

    }
}