package cc.cloudist.acplibrary;

import cc.cloudist.acplibrary.views.CustomView;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Collections;


public final class ACProgressCustom extends ACProgressBaseDialog {

    private static final int NO_TYPE = -1;
    private static final int RESOURCE_TYPE = 0;
    private static final int FILE_TYPE = 1;

    private Builder mBuilder;
    private CustomView mCustomView;

    private Timer mTimer;
    private int mSpinCount = 0;
    private int mResourceCount;

    private List<PixelMap> mBitmaps;
    private boolean CanceledOnTouchOutside;

    public boolean isCanceledOnTouchOutside() {
        return CanceledOnTouchOutside;
    }

    public void setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
        CanceledOnTouchOutside = canceledOnTouchOutside;
    }

    private ACProgressCustom(Builder builder) {
        super(builder.mContext);
        mBuilder = builder;


    }

    public void show(ACProgressCustom acProgressCustom) {
        if (mBuilder.mType != NO_TYPE) {
            if (mCustomView == null) {
                mBitmaps = new ArrayList<>();
                int size = (int) (getMinimumSideOfScreen(mBuilder.mContext) * mBuilder.mSizeRatio);
                if (mBuilder.mType == RESOURCE_TYPE) {
                    mResourceCount = mBuilder.mResources.size();
                    for (int i = 0; i < mResourceCount; i++) {
                        try {
                            mBitmaps.add(Utils.getPixelMapFromMedia(mBuilder.mContext,mBuilder.mResources.get(i)));
                        } catch (NotExistException | WrongTypeException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    mResourceCount = mBuilder.mFilePaths.size();
                    for (int i = 0; i < mResourceCount; i++) {
                        try {
                            mBitmaps.add(Utils.getPixelMapFromMedia(mBuilder.mContext,mBuilder.mResources.get(i)));
                        } catch (NotExistException | WrongTypeException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mCustomView = new CustomView(mBuilder.mContext,  mBitmaps);
            }
            setContentCustomComponent(mCustomView);
            this.setTransparent(true);
            super.show();
            mCustomView.setTouchEventListener(new Component.TouchEventListener() {
                @Override
                public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                    if (CanceledOnTouchOutside) {
                        acProgressCustom.hide();
                        mTimer.cancel();
                    }

                    return false;
                }
            });
            long delay = (long) (1000 / mBuilder.mSpeed);
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    int result = mSpinCount % mResourceCount;
                    mCustomView.updateIndex(result);
                    if (result == 0) {
                        mSpinCount = 1;
                    } else {
                        mSpinCount++;
                    }
                }
            }, delay, delay);
        }


    }


    public static class Builder {

        private final Context mContext;

        private float mSizeRatio = 0.2f;

        private final List<Integer> mResources = new ArrayList<>();
        private final List<String> mFilePaths = new ArrayList<>();

        private int mType = NO_TYPE;

        private float mSpeed = 6.67f;

        public Builder(Context context) {
            mContext = context;
        }

        public Builder sizeRatio(float ratio) {
            mSizeRatio = ratio;
            return this;
        }

        public Builder speed(float speed) {
            mSpeed = speed;
            return this;
        }

        public Builder useImages(Integer... imageIds) {
            if (imageIds != null && imageIds.length != 0) {
                mResources.clear();
                Collections.addAll(mResources, imageIds);
                mType = RESOURCE_TYPE;
            }
            return this;
        }


        public ACProgressCustom build() {
            return new ACProgressCustom(this);
        }

    }
}
