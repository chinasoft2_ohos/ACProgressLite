
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cc.cloudist.acplibrary;

import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;

/**
 * Utils 图片id转PixelMap
 *
 * @author name
 * @since 2021-04-22
 */
public class Utils {
    /**
     * 我不知道工具类为什么要写私有的构造方法，但是写了checkStyle不会报错
     */
    private Utils() {
    }

    /**
     * 图片id转PixelMap
     *
     * @param context Context
     * @param mediaId mediaId
     * @return PixelMap
     * @throws NotExistException NotExistException
     * @throws WrongTypeException WrongTypeException
     * @throws IOException IOException
     */
    public static PixelMap getPixelMapFromMedia(Context context, int mediaId)
            throws NotExistException, WrongTypeException, IOException {
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        ResourceManager manager = context.getResourceManager();
        String path = manager.getMediaPath(mediaId);
        Resource resource = manager.getRawFileEntry(path).openRawFile();
        ImageSource source = ImageSource.create(resource, options);
        return source.createPixelmap(decodingOptions);
    }

    /**
     * getScreenWidth
     *
     * @param context context
     * @return Width
     */
    public static float getScreenWidth(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point size = new Point();
        display.get().getSize(size);
        return size.getPointX();
    }

    /**
     * getScreenHeight
     *
     * @param context context
     * @return Height
     */
    public static float getScreenHeight(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point size = new Point();
        display.get().getSize(size);
        return size.getPointY();
    }
}
