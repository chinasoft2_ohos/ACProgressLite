package cc.cloudist.acplibrary.components;

/**
 * coordinate to draw a petal in flower
 */
public class PetalCoordinate {

    private final int startX;
    private final int startY;
    private final int endX;
    private final int endY;

    public PetalCoordinate(int startX, int startY, int endX, int endY) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public int getEndX() {
        return endX;
    }

    public int getEndY() {
        return endY;
    }
}
