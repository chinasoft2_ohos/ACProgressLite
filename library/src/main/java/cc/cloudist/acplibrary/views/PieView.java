package cc.cloudist.acplibrary.views;


import cc.cloudist.acplibrary.Utils;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;


public final class PieView extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    private final RectFloat mBackgroundRect;
    private final RectFloat mPieRect;
    private final Paint mBackgroundPaint;
    private final Paint mRingPaint;
    private final Paint mPiePaint;

    private final int mSize;
    private final float mBackgroundCornerRadius;
    private final float mRingBorderPadding;

    private float mAngle;
    private final PieUpdateHandler mHandler;
    private final Context mContext;
    private final int height;
    private final int width;
    public PieView(
            Context context, int size, int bgColor, float bgAlpha, float bgCornerRadius
            , float ringBorderPadding
            , int ringThickness, int ringColor, float ringAlpha
            , int pieColor, float pieAlpha) {
        super(context);
        mHandler = new PieUpdateHandler(this);
        this.mContext=context;
        mSize = size;
        mBackgroundCornerRadius = bgCornerRadius;
        mRingBorderPadding = ringBorderPadding;
         height= (int) Utils.getScreenHeight(mContext);
        width= (int) Utils.getScreenWidth(mContext);
       float left= (new BigDecimal(width).subtract(new BigDecimal(size)) ).divide(new BigDecimal(2)).floatValue();
        float top= (new BigDecimal(height).subtract(new BigDecimal(size)) ).divide(new BigDecimal(2)).floatValue();
        float right= (new BigDecimal(width).add(new BigDecimal(size)) ).divide(new BigDecimal(2)).floatValue();
        float bottom= (new BigDecimal(height).add(new BigDecimal(size)) ).divide(new BigDecimal(2)).floatValue();

        mBackgroundRect = new RectFloat(left,top,right,bottom);
        float R= new BigDecimal(mSize).multiply(new BigDecimal(1).subtract(new BigDecimal(mRingBorderPadding))).divide(new BigDecimal(2)).subtract(new BigDecimal(10)).floatValue();
        float leftPie= new BigDecimal(width).divide(new BigDecimal(2)).subtract(new BigDecimal(R)).floatValue();
        BigDecimal divide = new BigDecimal(height).divide(new BigDecimal(2));
        float topPie= divide.subtract(new BigDecimal(R)).floatValue();
        float rightPie= new BigDecimal(width).divide(new BigDecimal(2)).add(new BigDecimal(R)).floatValue();
        float bottomPie= divide.add(new BigDecimal(R)).floatValue();
        mPieRect =new RectFloat(leftPie,topPie,rightPie,bottomPie);;
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setColor(new Color(bgColor));
        mBackgroundPaint.setAlpha((int) (bgAlpha * 255));

        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mRingPaint.setStrokeWidth(ringThickness);
        mRingPaint.setColor(new Color(ringColor));
        mRingPaint.setAlpha((int) (ringAlpha * 255));
        mRingPaint.setStyle(Paint.Style.STROKE_STYLE);

        mPiePaint = new Paint();
        mPiePaint.setAntiAlias(true);
        mPiePaint.setColor(new Color(pieColor));
        mPiePaint.setAlpha((int) (pieAlpha * 255));
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    public void updateAngle(float angle) {
        mAngle = angle;
        mContext.getUITaskDispatcher().asyncDispatch(mHandler::handleMessage);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        mBackgroundPaint.setAlpha(0.3f);
        canvas.drawRoundRect(mBackgroundRect, mBackgroundCornerRadius, mBackgroundCornerRadius, mBackgroundPaint);
        canvas.drawCircle(new BigDecimal(width/2).floatValue()  , new BigDecimal(height / 2).floatValue(),  new BigDecimal(mSize).multiply(new BigDecimal(1).subtract(new BigDecimal(mRingBorderPadding))).divide(new BigDecimal(2)).floatValue(), mRingPaint);
        canvas.drawArc(mPieRect, new Arc(-90, mAngle, true), mPiePaint);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.PRECISE));
        return true;
    }

    private static class PieUpdateHandler {
        WeakReference<PieView> reference;

        public PieUpdateHandler(PieView pieView) {
            reference = new WeakReference<>(pieView);
        }


        public void handleMessage() {
            PieView flowerView = reference.get();
            if (flowerView != null) {
                flowerView.invalidate();
            }
        }
    }

}
