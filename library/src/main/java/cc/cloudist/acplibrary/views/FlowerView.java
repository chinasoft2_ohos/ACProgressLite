package cc.cloudist.acplibrary.views;

import cc.cloudist.acplibrary.Utils;
import cc.cloudist.acplibrary.components.FlowerDataCalc;
import cc.cloudist.acplibrary.components.PetalCoordinate;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.List;

public final class FlowerView extends Component implements Component.EstimateSizeListener,
        Component.DrawTask {

    private int mSize;
    private int mPetalCount;
    private float mBackgroundCornerRadius;

    private RectFloat mBackgroundRect;
    private Paint mBackgroundPaint, mPetalPaint, mTextPaint;

    private List<PetalCoordinate> mPetalCoordinates;
    private int[] mPetalColors;

    private int mCurrentFocusIndex;

    private String mText;
    private final Context mContext;
    private int mTextHeight;
    private int mTextMarginTop;
    private final FlowerUpdateHandler mHandler;
    private final int windowWidth;
    private int height;

    public FlowerView(

            Context context, int size, int windowWidth, int bgColor, float bgAlpha, float bgCornerRadius
            , int petalThickness, int petalCount, float petalAlpha, float borderPadding, float centerPadding
            , int themeColor, int fadeColor
            , String text, int textSize, int textColor, float textAlpha, int textMarginTop) {
        super(context);
        setEstimateSizeListener(this);
        this.windowWidth = windowWidth;
        addDrawTask(this);
        mHandler = new FlowerUpdateHandler(this);
        mTextMarginTop = textMarginTop;
        this.mContext = context;
        init(size, bgColor, bgAlpha, bgCornerRadius,
                petalThickness, petalCount, petalAlpha, borderPadding, centerPadding, themeColor, fadeColor,
                text, textSize, textColor, textAlpha);
    }

    private void init(
            int size, int bgColor, float bgAlpha, float bgCornerRadius
            , int petalThickness, int petalCount, float petalAlpha, float borderPadding, float centerPadding
            , int themeColor, int fadeColor
            , String text, int textSize, int textColor, float textAlpha) {



        this.setClipEnabled(false);

        mSize = size;
        mPetalCount = petalCount;
        mBackgroundCornerRadius = bgCornerRadius;

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        Color color = new Color(bgColor);
        mBackgroundPaint.setColor(color);
        mBackgroundPaint.setAlpha(bgAlpha);

        mPetalPaint = new Paint();
        mPetalPaint.setAntiAlias(true);
        mPetalPaint.setStrokeWidth(petalThickness);
        mPetalPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);

        if (text != null && text.length() != 0) {
            mText = text;
            mTextPaint = new Paint();
            mTextPaint.setAntiAlias(true);
            mTextPaint.setColor(new Color(textColor));
            mTextPaint.setAlpha(textAlpha);
            mTextPaint.setTextSize(textSize);
            mTextPaint.getTextBounds(text);
            mTextPaint.setTextAlign(TextAlignment.CENTER);
            mTextHeight = mTextPaint.getTextBounds(text).getHeight();
        } else {
            mTextMarginTop = 0;
        }


        height = (int) Utils.getScreenHeight(mContext);
        mBackgroundRect = new RectFloat(new BigDecimal((windowWidth - mSize)/2).subtract(new BigDecimal(mTextHeight)).floatValue(),
                new BigDecimal(height / 2).subtract(new BigDecimal(size)).floatValue(),
                new BigDecimal((windowWidth - mSize+20) / 2).add(new BigDecimal(mSize+mTextHeight)).floatValue(),new BigDecimal (height / 2 + mTextHeight + mTextMarginTop).floatValue());

        FlowerDataCalc calc = new FlowerDataCalc(petalCount);
        mPetalCoordinates = calc.getSegmentsCoordinates(mSize, (int) (borderPadding * mSize), (int) (centerPadding * mSize), petalCount, windowWidth);
        mPetalColors = calc.getSegmentsColors(themeColor, fadeColor, petalCount, (int) (petalAlpha * 255));

    }

    public void updateFocusIndex(int index) {
        mCurrentFocusIndex = index;
        mContext.getUITaskDispatcher().asyncDispatch(mHandler::handleMessage);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawRoundRect(mBackgroundRect, mBackgroundCornerRadius, mBackgroundCornerRadius, mBackgroundPaint);
        PetalCoordinate coordinate;
        for (int i = 0; i < mPetalCount; i++) {
            coordinate = mPetalCoordinates.get(i);
            int index = (mCurrentFocusIndex + i) % mPetalCount;
            mPetalPaint.setColor(new Color(mPetalColors[index]));

            canvas.drawLine(coordinate.getStartX(), (new  BigDecimal(height / 2).subtract (new BigDecimal(coordinate.getStartY()) ).floatValue()), coordinate.getEndX(),
                    new  BigDecimal(height / 2).subtract (new BigDecimal(coordinate.getEndY())) .floatValue(), mPetalPaint);


        }
        if (mText != null) {
            canvas.drawText(mTextPaint, mText, (float) windowWidth / 2, (float) height / 2);
        }
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.PRECISE));
        return true;
    }


    private static class FlowerUpdateHandler {
        WeakReference<FlowerView> reference;

        public FlowerUpdateHandler(FlowerView flowerView) {
            reference = new WeakReference<>(flowerView);
        }


        public void handleMessage() {
            FlowerView flowerView = reference.get();
            if (flowerView != null) {
                flowerView.invalidate();
            }
        }
    }

}
