package cc.cloudist.acplibrary.views;


import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.util.List;


public class CustomView extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    private List<PixelMap> mBitmaps;
    private int mCurrentIndex = 0;
    private final CustomUpdateHandler mHandler;
    private final Paint paint ;
    public CustomView(Context context,  List<PixelMap> bitmaps) {
        super(context);

        mHandler = new CustomUpdateHandler(this);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(60);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        mBitmaps = bitmaps;
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    public void updateIndex(int index) {
        mCurrentIndex = index;
        mContext.getUITaskDispatcher().asyncDispatch(mHandler::handleMessage);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        PixelMap pixelMap = mBitmaps.get(mCurrentIndex);
        RectFloat  centerRectFloat = new RectFloat();
        int width = getWidth();
        int height=getHeight();
        int center = width / 2;
        int inRadius = center - 60;
        double length = inRadius - Math.sqrt(2) * 1.0f / 2 * inRadius;
        centerRectFloat.left = new BigDecimal(width/2).subtract(new BigDecimal(length)).floatValue();
        centerRectFloat.top = new BigDecimal (height/2 ).subtract(new BigDecimal(length)).floatValue();
        centerRectFloat.bottom =new BigDecimal (height/2).add((new BigDecimal(length))).floatValue();
        centerRectFloat.right = new BigDecimal(width/2).add((new BigDecimal(length))).floatValue();
        canvas.drawPixelMapHolderRect(new PixelMapHolder(pixelMap), centerRectFloat, paint);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimateConfig);
        int height = Component.EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.PRECISE));
        return true;

    }


    private static class CustomUpdateHandler {
        WeakReference<CustomView> reference;

        public CustomUpdateHandler(CustomView customView) {
            reference = new WeakReference<>(customView);
        }


        public void handleMessage() {
            CustomView customView = reference.get();
            if (customView != null) {
                customView.invalidate();
            }
        }
    }
}
