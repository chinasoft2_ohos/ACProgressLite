# ACProgressLite


#### 项目介绍

- 项目名称：ACProgressLite
- 所属系列：openharmony第三方组件适配移植
- 功能：openharmony 加载控件库，简洁、易用、可定制性强。用于快速实现类似 iOS 的 “加载中” 等弹出框。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：V1.2.1



#### 演示效果
<p align="center">
  <img src="image/demo.gif"   />
</p>




#### 安装教程

项目需要添加maven仓库，并且再build.gradle文件下添加依赖

 ```gradle
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}

// 添加依赖库
dependencies {

implementation('com.gitee.chinasoft_ohos:ACProgressLite:1.0.0')
    ......
}

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
使用该库非常简单，只需查看提供的示例的源代码。
```java
    ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show(dialog);
```
 例子

* 花瓣类型

```
ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .text("Title is here)
                        .fadeColor(Color.DKGRAY).build();
dialog.show(dialog);
```

* 圆饼类型

```
ACProgressPie dialog = new ACProgressPie.Builder(this)
                     .ringColor(Color.WHITE)
                     .pieColor(Color.WHITE)
                     .updateType(ACProgressConstant.PIE_AUTO_UPDATE)
                     .build();
dialog.show(dialog);
```

* 自定义类型

```
ACProgressCustom dialog = new ACProgressCustom.Builder(this)
                        .useImages(R.drawable.p0, R.drawable.p1, R.drawable.p2, R.drawable.p3)
                        .build();
dialog.show(dialog);
```
 配置说明
&emsp;目前提供3种类型的加载框：<br/>
&emsp;下面是一些通用的配置：
1.  `sizeRatio` 背景的大小。值应为小于1的浮点数，表示以屏幕较短一边的相应比例为框体大小，即：
背景边长 = 屏幕较短一边的长度 * sizeRatio
需要注意的是花瓣类型带文字的情况，下文有详细说明

1.  `bgColor` 背景的颜色，整型值。
1.  `bgAlpha` 背景的透明度，0为全透明，1为不透明，其他透明度设置也类似。
1.  `bgCornerRadius` 背景四个圆角的半径。

*其中 bgColor、bgAlpha、bgCornerRadius 不适用于自定义类型*


* **花瓣类型**
最常见的类型，支持标题文字显示

设置|说明
:------|:------------------
themeColor|花瓣起始颜色
borderPadding|花瓣外沿与背景边缘的距离占背景边长的比例（背景边长指的是根据sizeRatio计算出来的长度）
centerPadding|花瓣内沿与背景中心的距离占背景边长的比例（背景边长指的是根据sizeRatio计算出来的长度）
fadeColor|花瓣终止颜色
petalCount|花瓣数量
petalAlpha|花瓣的透明度
petalThickness|花瓣的粗细
direction|花瓣的旋转方向，顺时针`ACProgressConstant.DIRECT_CLOCKWISE`或逆时针`DIRECT_ANTI_CLOCKWISE`
speed|旋转速度，每秒的帧数
text|文本标题，显示在花瓣下方
textSize|文字大小
textColor|文字颜色
textAlpha|文字透明度
textMarginTop|文字与花瓣之间的距离

* **圆饼类型**
适合显示进度，支持自动更新进度或者手动更新进度。

设置|说明
:------|:------------------
ringColor|圆环的颜色
ringAlpha|圆环的透明度
ringThickness|圆环的粗细
ringBorderPadding|圆环与背景外沿与背景边缘的距离占背景边长的比例
pieColor|圆饼的颜色
pieAlpha|圆饼的透明度
pieRingDistance|圆饼与圆环的距离占背景边长的比例
updateType|更新模式。自动更新`PIE_AUTO_UPDATE`或者手动更新`PIE_MANUAL_UPDATE`。手动更新需要调用`setPiePercentage()`。
speed|自动更新模式下每秒的帧数
pieces|自动更新模式下圆饼被切分的块数


* **自定义类型**
类似于 GIF，支持 res/drawable 资源数组 或者 图片文件数组 作为数据源。

设置|说明
:------|:------------------
useImages|使用的图片资源 ID
useFiles|使用的图片文件对象
speed|每秒的帧数

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

1.0.0

#### 版权和许可信息

* MIT License
