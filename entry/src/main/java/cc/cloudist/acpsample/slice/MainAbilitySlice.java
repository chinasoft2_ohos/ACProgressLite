package cc.cloudist.acpsample.slice;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressCustom;
import cc.cloudist.acplibrary.ACProgressFlower;
import cc.cloudist.acplibrary.ACProgressPie;
import cc.cloudist.acpsample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initBtn();
    }

    private void initBtn() {
        findComponentById(ResourceTable.Id_button_1).setClickedListener(this);
        findComponentById(ResourceTable.Id_button_2).setClickedListener(this);
        findComponentById(ResourceTable.Id_button_3).setClickedListener(this);
        findComponentById(ResourceTable.Id_button_4).setClickedListener(this);
        findComponentById(ResourceTable.Id_button_5).setClickedListener(this);

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_button_1: {
                ACProgressFlower dialog = new ACProgressFlower.Builder(this)

                        .build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show(dialog);
            }

            break;
            case ResourceTable.Id_button_2: {
                ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                        .direction( ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(0xFF00FF00)
                        .fadeColor(0xFFFF0000)
                        .build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show(dialog);
            }
            break;
            case ResourceTable.Id_button_3: {
                ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                        .text("Text is here")
                        .build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show(dialog);
            }
            break;
            case ResourceTable.Id_button_4: {
                ACProgressPie dialog = new ACProgressPie.Builder(this).build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show(dialog);
            }
            break;
            case ResourceTable.Id_button_5: {
                ACProgressCustom dialog = new ACProgressCustom.Builder(this)
                        .useImages(ResourceTable.Media_p0, ResourceTable.Media_p1, ResourceTable.Media_p2, ResourceTable.Media_p3).build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show(dialog);
            }
            break;

        }

    }

}
