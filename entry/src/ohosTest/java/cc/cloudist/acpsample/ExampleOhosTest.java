/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.cloudist.acpsample;
import cc.cloudist.acplibrary.ACProgressFlower;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
/**
 * 测试
 *
 * @author wz
 * @since 2021-04-08
 */
public class ExampleOhosTest {
    static ACProgressFlower.Builder builder;
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("cc.cloudist.acpsample", actualBundleName);
    }

    @BeforeClass
    public static void set() {
        Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        builder = new ACProgressFlower.Builder(ability.getContext());

    }

    @Test
    public void testBackgroundColor() {
        builder.setBackgroundColor(Color.BLACK.getValue());
        assertEquals(Color.BLACK.getValue(), builder.getBackgroundColor());
    }
    @Test
    public void testBackgroundAlpha() {
        builder.setBackgroundAlpha(0.1f);
        assertEquals(0.1f, builder.getBackgroundAlpha(),0.2f);
    }
    @Test
    public void testBackgroundCornerRadius() {
        builder.setBackgroundCornerRadius(0.2f);
        assertEquals(0.1f, builder.getBackgroundCornerRadius(),0.2f);
    }
    @Test
    public void testText() {
        builder.setText("test");
        assertEquals("test", builder.getText());
    }
    @Test
    public void testFadeColor() {
        builder.setFadeColor((Color.RED.getValue()));
        assertEquals(Color.RED.getValue(), builder.getFadeColor());
    }
    @Test
    public void testSpeed() {
        builder.setSpeed(100f);
        assertEquals(100f, builder.getSpeed(),1f);
    }
    @Test
    public void testTextColor() {
        builder.setTextColor(Color.BLUE.getValue());
        assertEquals(Color.BLUE.getValue(), builder.getTextColor());
    }
    @Test
    public void testTextSize() {
        builder.setTextSize(100);
        assertEquals(100, builder.getTextSize());
    }
    @Test
    public void testThemeColor() {
        builder.setThemeColor(Color.CYAN.getValue());
        assertEquals(Color.CYAN.getValue(), builder.getThemeColor());
    }
    @Test
    public void testPetalCount() {
        builder.setPetalCount(200);
        assertEquals(200, builder.getPetalCount());
    }
}